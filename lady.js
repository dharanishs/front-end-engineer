
const resultsContainer = document.getElementById("results-container");
const loadingIndicator = document.getElementById("loading-indicator");

let page = 1;
let isLoading = false;

async function fetchData() {
    if (isLoading) return;
    
    isLoading = true;
    loadingIndicator.style.display = "block";

    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=25`);
        const data = await response.json();

        data.forEach(item => {
            const resultItem = document.createElement("div");
            resultItem.classList.add("result-item");
            resultItem.textContent = item.title;
            resultsContainer.appendChild(resultItem);
        });

        page++;
    } catch (error) {
        console.error("Error fetching data:", error);
    } finally {
        isLoading = false;
        loadingIndicator.style.display = "none";
    }
}


function handleScroll() {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        fetchData();
    }
}


fetchData();


window.addEventListener("scroll", handleScroll);
